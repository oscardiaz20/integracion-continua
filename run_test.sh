#!/bin/sh

# Salir inmediatamente si un comando falla
set -e

# Construir el proyecto y ejecutar pruebas
mvn clean test
